---
- name: Chroot config
  block:

    - name: Packages present
      ansible.builtin.apt:
        pkg:
          - bash-completion
          - bc
          - composer
          - curl
          - dnsutils
          - dos2unix
          - git
          - gnupg
          - jq
          - kitty-terminfo
          - less
          - libmagickcore-6.q16-3-extra
          - libxml-xpath-perl
          - libxml2-utils
          - lynx
          - mariadb-client
          - mutt
          - mycli
          - nano
          - php-cli
          - php-mysql
          - prips
          - pwgen
          - python3
          - python3-pip
          - python3-pygments
          - ssl-cert-check
          - subversion
          - sudo
          - texlive
          - wget
          - unzip
          - vim
        update_cache: true

    - name: Packages absent
      ansible.builtin.apt:
        pkg:
          - cron
        state: absent
        autoclean: true
        autoremove: true

    - name: Check if mini_sendmail is available
      ansible.builtin.stat:
        path: /usr/local/sbin/mini_sendmail
      register: chroot_config_mini_sendmail_check

    - name: Remove Exim4 and install mini_sendmail
      block:

        - name: Exim packages absent
          ansible.builtin.apt:
            pkg:
              - exim4-base
              - exim4-config
              - exim4-daemon-light
              - bsd-mailx
              - apticron
            state: absent

        - name: Symlink mini_sendmail
          ansible.builtin.file:
            src: /usr/local/sbin/mini_sendmail
            dest: /usr/sbin/sendmail
            state: link
            follow: false
            mode: "0777"
            owner: root
            group: root

      when: chroot_config_mini_sendmail_check.stat.exists | bool

    - name: /run/mysqld directory present
      ansible.builtin.file:
        path: /run/mysqld
        state: directory
        mode: "0755"

    - name: /run/chroot directory present
      ansible.builtin.file:
        path: /run/chroot
        state: directory
        mode: "0755"

    - name: Apt autoclean and autoremove
      ansible.builtin.apt:
        autoclean: true
        autoremove: true

    - name: Find log files and directories
      ansible.builtin.find:
        path: /var/log
        file_type: any
        recurse: true
        excludes:
          - apache2
      register: chroot_logs

    - name: Debug chroot_logs
      ansible.builtin.debug:
        var: path.path
        verbosity: 3
      loop_control:
        loop_var: path
        label: "{{ path.path | basename }}"
      loop: "{{ chroot_logs.files }}"

    - name: Log files and directories absent
      ansible.builtin.file:
        path: "{{ path.path }}"
        state: absent
      loop_control:
        loop_var: path
        label: "{{ path.path | basename }}"
      loop: "{{ chroot_logs.files }}"
      when: chroot_logs.files != []
      changed_when: false  # for idempotence

    - name: Root cron spool file absent
      ansible.builtin.file:
        path: /var/spool/cron/crontabs/root
        state: absent

    - name: Apache suEXEC config directory only readable by root
      ansible.builtin.file:
        path: /etc/apache2/suexec
        state: directory
        mode: "0700"

  tags:
    - chroot
...
