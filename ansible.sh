#!/usr/bin/env bash

# Copyright 2019-2024 Chris Croome
#
# This file is part of the Webarchitects chroot-config Ansible repo.
#
# The Webarchitects chroot-config Ansible repo.is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects chroot-config Ansible repo.is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects chroot-config Ansible repo. If not, see <https://www.gnu.org/licenses/>.

set -e -o pipefail

export ANSIBLE_CONFIG="ansible.cfg"
export DEBIAN_FRONTEND="noninteractive"
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
if [[ "$(id -u)" == "0" ]]
then
  export PIPX_BIN_DIR="/usr/local/bin"
  export PIPX_HOME="/opt/pipx"
  if [[ -d /root/.ansible ]]
  then
    echo "Deleting /root/.ansible"
    rm -rf /root/.ansible
  fi
  if [[ -d /root/.local/pipx ]]
  then
    echo "Deleting /root/.local/pipx"
    rm -rf /root/.local/pipx
  fi
  if [[ -d /opt/pipx ]]
  then
    echo "Deleting /opt/pipx"
    rm -rf /opt/pipx
  fi
  if [[ -d /opt/shared ]]
  then
    echo "Deleting /opt/shared"
    rm -rf /opt/shared
  fi
  if [[ -d /opt/venvs ]]
  then
    echo "Deleting /opt/venvs"
    rm -rf /opt/venvs
  fi
  if [[ -d /opt/.cache ]]
  then
    echo "Deleting /opt/.cache"
    rm -rf /opt/.cache
  fi
fi

distro=$(grep '^ID=' /etc/os-release | awk -F "=" '{print $2}')
version=$(grep '^VERSION_CODENAME=' /etc/os-release | awk -F "=" '{print $2}')

if [[ "${distro}" == "debian" ]]
then
# Bookworm ships python3-psycopg for psycopg3
if [[ "${version}" == "bookworm" || "${version}" == "trixie" ]]
then
requirements="ansible
ansible-lint
git
jc
jo
libpq-dev
pipx
postgresql-client
postgresql-common
procps
python3-argcomplete
python3-commonmark
python3-debian
python3-jmespath
python3-lxml
python3-netaddr
python3-pip
python3-psycopg
python3-resolvelib
python3-ruamel.yaml
python3-setuptools
python3-venv"
else
requirements="ansible
ansible-lint
git
jc
jo
libpq-dev
pipx
postgresql-client
postgresql-common
procps
python3-argcomplete
python3-commonmark
python3-debian
python3-jmespath
python3-lxml
python3-netaddr
python3-pip
python3-psycopg2
python3-resolvelib
python3-ruamel.yaml
python3-setuptools
python3-venv"
fi
elif [[ "${distro}" == "ubuntu" ]]
then
requirements="ansible
ansible-lint
git
jc
jo
pipx
postgresql
procps
python3-argcomplete
python3-commonmark
python3-debian
python3-jmespath
python3-lxml
python3-netaddr
python3-pip
python3-psycopg2
python3-resolvelib
python3-ruamel.yaml
python3-setuptools
python3-venv"
else
  echo "This script only supports debian and ubuntu"
  exit
fi


if [[ "$(id -u)" != "0" ]];
then
  for req in ${requirements}; do
    dpkg -s "${req}" &>/dev/null || (>&2 echo "Please run: sudo apt install ${req}" ; exit 1)
  done
elif [[ "$(id -u)" == "0" ]];
then
  # shellcheck disable=SC2086
  apt install -y ${requirements} || exit
fi

md5sum_before="$(md5sum "${0}" | awk '{ print $1 }')"

git pull || exit

md5sum_after="$(md5sum "${0}" | awk '{ print $1 }')"

if [[ "${md5sum_after}" != "${md5sum_before}" ]]; then
  echo "The ${0} script has been updated, please run it again."
  exit
fi

pipx_version=$(pipx --version)
# pipx environment was added in version 1.1.0
pipx_test_version=$(printf "${pipx_version}\n1.1.0" | sort --version-sort --reverse | tail -n1)
if [[ "${pipx_test_version}" != "1.1.0" ]]
then
  if [[ "$(id -u)" == "0" ]];
  then
    pipx_local_venvs="/opt/pipx/venvs"
  else
    pipx_local_venvs="${HOME}/.local/pipx/venvs"
  fi
else
  pipx_local_venvs=$(pipx environment | grep ^PIPX_LOCAL_VENVS | sed 's/PIPX_LOCAL_VENVS=//')
fi

/usr/bin/ansible-galaxy install -r requirements.yml --force ansible || exit

# Delete the ansible venv
if [[ "${1}" != "--check" && "${1}" != "-C" ]];
then
  if [[ -d "${pipx_local_venvs}/ansible" ]];
  then
    echo "In 10 seconds the existing Ansible venv at ${pipx_local_venvs}/ansible will be deleted, prior to reinstalling, hit Ctrl-C to prevent this!"
    sleep 10
    rm -rf "${pipx_local_venvs}/ansible"
  fi
  echo "Reinstalling pipx"
  pipx reinstall-all
else
  if [[ -d "${pipx_local_venvs}/ansible" ]];
  then
    echo "If this script was not running in check mode the pipx venv at ${pipx_local_venvs}/ansible would be deleted prior to running the ansible role"
  fi
fi

if [[ "${#}" -eq 0 ]]; then
  /usr/bin/ansible-playbook ansible.yml --diff
elif [[ "${1}" == "--check" || "${1}" == "-C" ]];
then
  /usr/bin/ansible-playbook ansible.yml --diff --check
elif [[ "${1}" == "--verbose" || "${1}" == "-v" ]];
then
  /usr/bin/ansible-playbook ansible.yml --diff -v
else
  echo "This script only supports the --check / -C or --verbose / -v arguments."
  exit 1
fi

