# Ansible Debian chroot configuration

The Ansible in this repo uses the chroot connection to configure a chroot on Debian.

This repo needs to be present on the server that has the chroot because the chroot connection cannot be used over SSH.

The chroot needs to already exist and [this role](https://git.coop/webarch/chroot) can be used to build a chroot in `/chroot` and it also clones this repo and runs the `run.sh` wrapper script.

The [ansible.cfg](ansible.cfg) in this repo results in the playbooks returning JSON output, which is required when they are run remotly via Ansible and the output is to be parsed, if you need to run the playbooks directly, for example when doing a distro upgrade, then best use the [ansible_human.cfg](ansible_human.cfg):

```bash
source ./human.sh
printenv ANSIBLE_CONFIG
```

## Copyright

Copyright 2019-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
