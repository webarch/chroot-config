#!/usr/bin/env bash

# Copyright 2019-2024 Chris Croome
#
# This file is part of the Webarchitects chroot-config Ansible repo.
#
# The Webarchitects chroot-config Ansible repo.is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects chroot-config Ansible repo.is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects chroot-config Ansible repo. If not, see <https://www.gnu.org/licenses/>.

set -e -o pipefail

export ANSIBLE_CONFIG="ansible.cfg"
export DEBIAN_FRONTEND="noninteractive"
export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

git pull  && \
  ansible-galaxy install -r requirements.yml --force apt upgrade && \
    ansible-playbook upgrade.yml
